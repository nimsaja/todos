//import React from 'react';
//import ReactDOM from 'react-dom';

import {
  addTodo,
  toggleTodo,
  setVisibilityFilter,
  VisibilityFilters
} from './actions'

import todoApp from './reducers';
import { createStore } from 'redux';

const store = createStore(
  todoApp,
  window.devToolsExtension ? window.devToolsExtension() : f => f
);

// Log the initial statef
console.log(store.getState());

const unsubscribe = store.subscribe(() =>
  console.log(store.getState())
)

// Dispatch some actions
store.dispatch(addTodo('Learn about actions'))
store.dispatch(addTodo('Learn about reducers'))
store.dispatch(addTodo('Learn about store'))
store.dispatch(toggleTodo(0))
store.dispatch(toggleTodo(1))
store.dispatch(setVisibilityFilter(VisibilityFilters.SHOW_COMPLETED))

// Stop listening to state updates
unsubscribe();

//ReactDOM.render(<div>index</div>, document.getElementById('root'));
