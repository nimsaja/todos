import { VisibilityFilters } from '../actions';

const initialState = {
  visibilityFilters: VisibilityFilters.SHOW_ALL,
  todos: []
}

export default function todoApp(state = initialState, action) {
  switch (action.type) {
    case 'ADD_TODO':
      return Object.assign({}, state, {
        todos: [
          ...state.todos,
          {
            text: action.text,
            completed: false
          }
        ]
      });
    break;
    case 'TOGGLE_TODO':
      return Object.assign({}, state, {
        todos: state.todos.map((todo, key) => {
          if(key === action.index) {
            return Object.assign({}, todo, {
              completed: ! todo.completed
            })
          }
          return todo;
        })
      }
      );
    break;
    case 'SET_VISIBILITY_FILTER':
      return Object.assign({}, state, {
        visibilityFilters: action.filter
      });
    break;
    default:
      return state;
  }
}
