import { combineReducers } from 'redux';
import { VisibilityFilters } from '../actions';

const { SHOW_ALL } = VisibilityFilters;

function todos(state = [], action) {
  switch (action.type) {
    case 'ADD_TODO':
      return [
        ...state,
        {
          text: action.text,
          completed: false
        }
      ]
      break;
    case 'TOGGLE_TODO':
      return state.map((todo, key) => {
        if(key === action.index) {
          return Object.assign({}, todo, {
            completed: ! todo.completed
          })
        }
        return todo;
      })
    break;
    default:
      return state;
  }
}

function visibilityFilters(state = SHOW_ALL, action) {
  switch (action.type) {
    case 'SET_VISIBILITY_FILTER':
        return action.filter
    break;
    default:
      return state;
  }
}

const todoApp = combineReducers({
  visibilityFilters,
  todos
})

export default todoApp;

/*export default function todoApp(state = {}, action) {
  return {
    visibilityFilters: visibilityFilters(state.visibilityFilters, action),
    todos: todos(state.todos, action)
  }

  /*switch (action.type) {
    case 'ADD_TODO':
      return Object.assign({}, state, {
        todos: todos(state.todos, action)
      });
    break;
    case 'TOGGLE_TODO':
      return Object.assign({}, state, {
        todos: todos(state.todos, action)
      }
      );
    break;
    case 'SET_VISIBILITY_FILTER':
      return Object.assign({}, state, {
        visibilityFilters: visibilityFilters(state.visibilityFilters, action),
      });
    break;
    default:
      return state;
  }*//*
}*/
